package openSDC.openSDCDevice.LatencyMeasurementDevice;

import java.io.File;
import java.util.logging.Level;

import util.MyLogger;

import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.util.MDIBFileReaderUtil;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescription;

public class MyBICEPSDeviceNodeInterfaceDescription implements BICEPSDeviceNodeInterfaceDescription {

	public static final String PATH_TO_XML_FILE = "description/LatencyMeasurementDevice.xml";
	
	private MDDescription description = null;
	private File xmlDescriptionFile = new File(PATH_TO_XML_FILE);
	
	private static MyBICEPSDeviceNodeInterfaceDescription dni = null;
	
	public static synchronized MyBICEPSDeviceNodeInterfaceDescription getInstance(){
		if (dni == null){
			dni = new MyBICEPSDeviceNodeInterfaceDescription();		
		}
		return dni;
	}
	
	public MDDescription getMDIB() {
		if(description != null){
			return description;
		}


		if(xmlDescriptionFile != null){
			description = new MDIBFileReaderUtil().getDescriptionFromFile(xmlDescriptionFile); //load the description
		} else {
			MyLogger.log.log(Level.WARNING, "Cannot open XML description file");
		}
		
		return description;
	}

	public File getXmlDescriptionFile() {
		return xmlDescriptionFile;
	}

	public void setXmlDescriptionFile(File xmlDescriptionFile) {
		this.xmlDescriptionFile = xmlDescriptionFile;
	}

}
