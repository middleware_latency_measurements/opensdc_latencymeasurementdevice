package openSDC.openSDCDevice.LatencyMeasurementDevice;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;








import util.MyLogger;
import util.Util;

import com.draeger.medical.biceps.common.model.ComponentActivation;
import com.draeger.medical.biceps.common.model.ComponentState;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricMeasurementState;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.NumericValue;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescription;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescriptionProvider;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceState;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceStateProvider;
import com.draeger.medical.biceps.device.mdi.impl.DefaultMDIStateBuilder;
import com.draeger.medical.biceps.device.mdi.interaction.MDIStateBuilder;


public class MyDeviceNodeInterface implements
		BICEPSDeviceNodeInterfaceDescriptionProvider,
		BICEPSDeviceNodeInterfaceStateProvider {
	

	public MyDeviceNodeInterface(){
		//make this object accessible for the connector (we use it to get the MDState)
//		RealDeviceConnector.setMdni(this);
	}
	
	private final BICEPSDeviceNodeInterfaceState medicalDeviceState=new BICEPSDeviceNodeInterfaceState() {
		
		private final MDState states=new MDState();
		private MDIStateBuilder stateBuilder=new DefaultMDIStateBuilder();
		private boolean initialStatesBuilt=false;
		
		


		public synchronized MDState getInitialMDState() {
			if (!initialStatesBuilt)
			{
				stateBuilder.initializeStatesForDescription(medicalDeviceDescription.getMDIB());
				MDState initialStates = stateBuilder.getMedicalDeviceInterfaceState();				
				List<State> stateList = initialStates.getStates();
				
				{

				states.setStates(stateList);
				
				for(State s: states.getStates())
				{
					System.err.println("--- " + s.getClass().getName());
					if (s instanceof OperationState) {
						((OperationState)s).setState(OperationalState.ENABLED);
					} else if (s instanceof ComponentState){
						ComponentState cs = (ComponentState) s;
						cs.setState(ComponentActivation.ON);
						
						 if(s instanceof NumericMetricState){
								System.err.println("### set observed value");
								NumericMetricState nms = (NumericMetricState) s;
								NumericValue nv = Util.createNumericValue(BigDecimal.ZERO, MetricMeasurementState.VALID);
								nms.setObservedValue(nv);
						}
					} 
					else {
						MyLogger.log.log(Level.FINE, "State found that is not enabled! (" + s.toString() + ")");
					}
				}
				}
						
				initialStatesBuilt=true;
			}
			return states;
		}


		public void setStateBuilder(MDIStateBuilder stateBuilder) 
		{
			this.stateBuilder=stateBuilder;
		}
		
	};
	
	private final BICEPSDeviceNodeInterfaceDescription medicalDeviceDescription = MyBICEPSDeviceNodeInterfaceDescription.getInstance();
	

	
	
	public BICEPSDeviceNodeInterfaceState getInterfaceState() {
		return medicalDeviceState;
	}

	public BICEPSDeviceNodeInterfaceDescription getInterfaceDescription() {
		return medicalDeviceDescription;
	}


	
}
