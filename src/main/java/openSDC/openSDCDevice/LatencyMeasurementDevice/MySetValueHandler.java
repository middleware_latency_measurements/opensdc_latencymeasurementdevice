package openSDC.openSDCDevice.LatencyMeasurementDevice;

import java.util.concurrent.BlockingQueue;

import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.impl.DefaultCommandHandler;
import com.draeger.medical.biceps.device.mdi.interaction.notification.CurrentValueChanged;
import com.draeger.medical.biceps.device.mdi.interaction.operation.ActivateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetAlertStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetContextStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetIdentifiableContextCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetRangeCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetStringCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetValueCommand;

public class MySetValueHandler extends DefaultCommandHandler {

	private final MyCommandProcessingTask cmdTask;

	public MySetValueHandler(MyCommandProcessingTask commandProcessingTask) {
		super(commandProcessingTask);
		this.cmdTask=commandProcessingTask;
	}


	@Override
	/**
	 * we have to declare all commands we will react to
	 * TODO: Attention this has to be done for _ALL_ commands ... !!!
	 */
	protected void fillHandledCommands() {
		getHandledCommands().add(SetValueCommand.class);
		
		//we have to add the activity command here
		getHandledCommands().add(ActivateCommand.class);
		
		//and add SetContextStateCommand
		getHandledCommands().add(SetContextStateCommand.class);
		
		//and add SetStringCommand
		getHandledCommands().add(SetStringCommand.class);
		
		getHandledCommands().add(SetAlertStateCommand.class);
		getHandledCommands().add(SetIdentifiableContextCommand.class);
		getHandledCommands().add(SetRangeCommand.class);

	}

	@Override
	protected void fillSendNotifications() {
		getSendNotifications().add(CurrentValueChanged.class);
	}

	@Override
	public void setMDINotificationQueue(
			BlockingQueue<MDINotification> notificationQueue) {
		super.setMDINotificationQueue(notificationQueue);
		this.cmdTask.setNotificationQueue(notificationQueue);
	}



}
