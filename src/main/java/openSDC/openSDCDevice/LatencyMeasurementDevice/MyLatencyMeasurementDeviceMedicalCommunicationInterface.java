package openSDC.openSDCDevice.LatencyMeasurementDevice;

import java.util.concurrent.BlockingQueue;

import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescriptionProvider;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceStateProvider;
import com.draeger.medical.biceps.device.mdi.impl.DefaultMedicalDeviceCommunicationInterface;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;

public class MyLatencyMeasurementDeviceMedicalCommunicationInterface extends DefaultMedicalDeviceCommunicationInterface{

		private final MyDeviceNodeInterface medicalDeviceNodeInterface=new MyDeviceNodeInterface();
		private final MyLatencyMeasurementDeviceCommandProcessingTask cmdProcTask=new MyLatencyMeasurementDeviceCommandProcessingTask(this);
		
		@Override
		public BICEPSDeviceNodeInterfaceDescriptionProvider getInterfaceDescriptionProvider() {
			return medicalDeviceNodeInterface;
		}
		
		@Override
		public BICEPSDeviceNodeInterfaceStateProvider getInterfaceStateProvider() {
			return medicalDeviceNodeInterface;
		}

		@Override
		protected void addHandlers() {
			addHandler(new MySetValueHandler(cmdProcTask));
		}

		@Override
		protected void addWaveformGenerators() {
//			addWaveformGenerator(new MyWaveformGenerator(TestAutomationDevice.HANDLE_STREAM, TestAutomationDevice.STREAM_FRAME_CAPACITY ));
		}

		@Override
		public void connect() 
		{
			System.out.println("Connected");
		}

		@Override
		public void disconnect() {
			System.out.println("Disconnected");
		}

		@Override
		public void startupComplete() {
			super.startupComplete();
		}

		@Override
		public void setMDINotificationQueue(
				BlockingQueue<MDINotification> notificationQueue) {
			super.setMDINotificationQueue(notificationQueue);
		}


}
