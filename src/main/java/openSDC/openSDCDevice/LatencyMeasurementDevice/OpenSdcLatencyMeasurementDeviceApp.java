package openSDC.openSDCDevice.LatencyMeasurementDevice;

import java.util.Scanner;

import org.ws4d.java.util.Log;

import uro.latencyMeasurement.LatencyMeasurement;

import com.draeger.medical.biceps.device.BICEPSDeviceApplication;
import com.draeger.medical.biceps.device.mdi.MedicalDeviceCommunicationInterface;

/**
 * My openSDC Device.
 * 
 * @author Parts or complete functionality copied from the openSDC Beta-01 tutorial and / or OR.NET openSDC tutorial workshop 
 * @author Martin Kasparick (University of Rostock - Institute of Applied Microelectronics and Computer Engineering)
 * @note Free for use in OR.NET context. (Please keep the original author(s) information in your projects.)
 */
public class OpenSdcLatencyMeasurementDeviceApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
				
		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		new BICEPSDeviceApplication(args) {
			MedicalDeviceCommunicationInterface medicalDeviceComInterface=new MyLatencyMeasurementDeviceMedicalCommunicationInterface();
			
			protected MedicalDeviceCommunicationInterface getMedicalDeviceCommunicationInterface() {
				return medicalDeviceComInterface;
			}
			
			protected int getConfigurationId() {
				return 1;
			}
		};



		BICEPSDeviceApplication.getApplication().run();
		
		//some interaction
		String input = ""; //will contain the console input
		Scanner scanner = new Scanner(System.in); //we will use it to read from the console
		
		while(!input.equals("x")) { //"x" as about input
			System.out.println("Insert command. (end program with \"x\"; clear measurements with \"c\"; write data into files with \"w\")");
			
			//read console input
			input = scanner.nextLine();
			
			//abort criterion
			if (input.equals("x")) {
				break; //leave the loop
			}
			
			if (input.equals("w")) {
				LatencyMeasurement.getInstance().writeEveryMeasurementToFiles(true);
			}
			
			if (input.equals("c")) {
				LatencyMeasurement.getInstance().clearAllMeasurements();
			}

		}
		
		scanner.close();


	}

}
