package openSDC.openSDCDevice.LatencyMeasurementDevice;

import java.math.BigDecimal;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import util.MyLogger;
import util.Util;

import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.EnumStringMetricState;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.MetricMeasurementState;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.SetString;
import com.draeger.medical.biceps.common.model.SetValue;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.StringMetricState;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.CurrentValueChanged;
import com.draeger.medical.biceps.device.mdi.interaction.notification.OperationStateChangedNotification;
import com.draeger.medical.biceps.device.mdi.interaction.operation.ActivateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.OperationCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetAlertStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetContextStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetStringCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetValueCommand;
import com.draeger.medical.biceps.device.mdib.MDIBOperation;
import com.draeger.medical.biceps.device.mdib.OperationRequest;


/**
 * 
 * @author Parts or complete functionality copied from the openSDC Beta-01 tutorial and / or OR.NET openSDC tutorial workshop 
 * @author Martin Kasparick (University of Rostock - Institute of Applied Microelectronics and Computer Engineering)
 * @note Free for use in OR.NET context. (Please keep the original author(s) information in your projects.)
 */
public class MyLatencyMeasurementDeviceCommandProcessingTask extends MyCommandProcessingTask {
	

	

	public MyLatencyMeasurementDeviceCommandProcessingTask(
			MyLatencyMeasurementDeviceMedicalCommunicationInterface medicalDeviceNodeInterface) 
	{
		setStates(medicalDeviceNodeInterface.getInterfaceStateProvider()
				.getInterfaceState().getInitialMDState());
		
		setDescription(medicalDeviceNodeInterface.getInterfaceDescriptionProvider()
				.getInterfaceDescription().getMDIB());
		
	}


	@Override
	protected void handleCommand(MDICommand command) 
	{
		
		if ((command instanceof SetValueCommand)
				|| (command instanceof ActivateCommand))
		{
			OperationCommand operationCommand=(OperationCommand) command;
			MDIBOperation mdibOp = operationCommand.getOperation();
			if (mdibOp!=null)
			{
				OperationRequest op = mdibOp.getOperationRequest();
				if (op!=null)
				{
					try {
						boolean operationHasNotFailed=false;

						//make a notification that we have started to handle the incoming command
						notificationQueue.offer(new OperationStateChangedNotification(mdibOp.getMdsHandle(), mdibOp, InvocationState.STARTED), 
								500, TimeUnit.MILLISECONDS);
						

						
						//now we are looking for the state that fits to the operation
						OperationDescriptor operationDescriptor = op.getOperationDescriptor();
						String target = operationDescriptor.getOperationTarget(); //now we have the handle of the target of the operation (have a look at the xml description of the device)
						
						//walking through the set of states and looking of the state that fits to the target handle
						for (State state : states.getStates()) {
							if (state.getReferencedDescriptor().equals(target)){
								if( ((command instanceof SetValueCommand)  && (state instanceof NumericMetricState))
										|| ((command instanceof SetStringCommand)  && (state instanceof StringMetricState) )) {
									AbstractMetricState ms=(AbstractMetricState)state; //cast it to a NumericMetricState
									
									//get the value to set from the request
									AbstractSet setValueMessage=(AbstractSet)op.getOperationMessage();
									if(state instanceof NumericMetricState){
										System.err.println("The setValueMessage contains this value: " + ((SetValue) setValueMessage).getValue());
									}
									else if(state instanceof StringMetricState){
										System.err.println("The setValueMessage contains this value: " + ((SetString) setValueMessage).getString());
										
										//if it is an 
										if(state instanceof EnumStringMetricState){
											
										}
									}
									

									//perform the change request in openSDC by inserting it into the queue
									if(changeSettingValue(setValueMessage, ms)){
										CurrentValueChanged cvcNotification=new CurrentValueChanged(ms);
										operationHasNotFailed |= notificationQueue.offer(cvcNotification);
									}


								} else if(command instanceof ActivateCommand) {
									NumericMetricState nms=(NumericMetricState)state; //cast it to a NumericMetricState
									//set new value
									nms.setObservedValue(Util.createNumericValue(BigDecimal.valueOf(0), MetricMeasurementState.VALID));
									CurrentValueChanged cvcNotification=new CurrentValueChanged(nms);
									if(notificationQueue != null){
										operationHasNotFailed = notificationQueue.offer(cvcNotification);
//										MyLogger.log.log(Level.FINER, "notification queued... ");
									}
									else{
										MyLogger.log.log(Level.SEVERE, "No notification queue found!");
									}
								}

								break; //we have found the state -> break the loop
							}
						}


						//make a notification that we finished handling the request (finished or failed)
						if (operationHasNotFailed){							
							notificationQueue.offer(new OperationStateChangedNotification(mdibOp.getMdsHandle(), mdibOp, InvocationState.FINISHED), 
									500, TimeUnit.MILLISECONDS);
//							MyLogger.log.log(Level.FINE, "Offer InvocationState.FINISHED");
							}
						else{							
							notificationQueue.offer(new OperationStateChangedNotification(mdibOp.getMdsHandle(), mdibOp, InvocationState.FAILED), 
									500, TimeUnit.MILLISECONDS);
//							MyLogger.log.log(Level.FINE, "Offer InvocationState.FAILED");
							}

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
		else if (command instanceof SetContextStateCommand) {
			MyLogger.log.log(Level.SEVERE, "SetContextCommand received. -> NOT implemented");

		}
		else if (command instanceof SetAlertStateCommand){
			MyLogger.log.log(Level.SEVERE, "SetAlertStateCommand received.");
			handleSetAlertStateCommand(command);
		}
	}

	
	public void setNotificationQueue(
			BlockingQueue<MDINotification> notificationQueue) {
		this.notificationQueue = notificationQueue;
	}

}
