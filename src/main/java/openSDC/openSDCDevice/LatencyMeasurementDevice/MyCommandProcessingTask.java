package openSDC.openSDCDevice.LatencyMeasurementDevice;

import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import util.MyLogger;

import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.AbstractMetricValue;
import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.ChannelDescriptor;
import com.draeger.medical.biceps.common.model.EnumStringMetricDescriptor;
import com.draeger.medical.biceps.common.model.EnumStringMetricState;
import com.draeger.medical.biceps.common.model.HydraMDSDescriptor;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MeasurementState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.MetricMeasurementState;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.NumericValue;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.SetString;
import com.draeger.medical.biceps.common.model.SetValue;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.StringMetricState;
import com.draeger.medical.biceps.common.model.StringMetricValue;
import com.draeger.medical.biceps.common.model.VMDDescriptor;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.impl.DefaultCommandHandler.CommandProcessingTask;
import com.draeger.medical.biceps.device.mdi.interaction.notification.AlertStateChanged;
import com.draeger.medical.biceps.device.mdi.interaction.notification.OperationStateChangedNotification;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetAlertStateCommand;
import com.draeger.medical.biceps.device.mdib.MDIBOperation;
import com.draeger.medical.biceps.device.mdib.OperationRequest;

/**
 * 
 * @author Parts or complete functionality copied from the openSDC Beta-01 tutorial and / or OR.NET openSDC tutorial workshop 
 * @author Martin Kasparick (University of Rostock - Institute of Applied Microelectronics and Computer Engineering)
 * @note Free for use in OR.NET context. (Please keep the original author(s) information in your projects.)
 */
public abstract class MyCommandProcessingTask extends CommandProcessingTask {

	protected MDState states = null;
	protected BlockingQueue<MDINotification> notificationQueue;
	protected MDDescription description = null;
	
	@Override
	abstract protected void handleCommand(MDICommand arg0);


	public void setNotificationQueue(
			BlockingQueue<MDINotification> notificationQueue) {
		this.notificationQueue = notificationQueue;
	}
	
	/**
	 * Method sets the new state given by the command. It also sends the OperationStateChangedNotifications
	 * @TODO TODO use a boolean return value and make the notification within another method (e.g. handleCommand)  
	 * @param command
	 */
	protected void handleSetAlertStateCommand(MDICommand command){
		SetAlertStateCommand setAlertStateCommand = (SetAlertStateCommand) command;
		
		MDIBOperation mdibOp = setAlertStateCommand.getOperation();
		if (mdibOp!=null)
		{
			OperationRequest op = mdibOp.getOperationRequest();
			if (op!=null)
			{
				try {
					boolean operationHasNotFailed=false;

					notificationQueue.offer(new OperationStateChangedNotification(mdibOp.getMdsHandle(), mdibOp, InvocationState.STARTED), 
							500, TimeUnit.MILLISECONDS);
					

					OperationDescriptor operationDescriptor = op.getOperationDescriptor();
					String target = operationDescriptor.getOperationTarget();
					MyLogger.log.log(Level.FINER, "The the target handle is: " + target);

					for (State state : states.getStates()) {
						if (state.getReferencedDescriptor().equals(target) ) //found the state for this operation
						{
							AbstractAlertState aas=(AbstractAlertState)state;
							
							AlertStateChanged scNotification = null;
							scNotification = new AlertStateChanged(aas);
							
							MyLogger.log.log(Level.FINE, "LocationContextState change offered");
							operationHasNotFailed |= (scNotification != null ? notificationQueue.offer(scNotification) : false);

							break;							
						}
					}
					
					if (operationHasNotFailed) {
						MyLogger.log.log(Level.FINER, "Finished setting new context state");
						notificationQueue.offer(new OperationStateChangedNotification(mdibOp.getMdsHandle(), mdibOp, InvocationState.FINISHED), 
								500, TimeUnit.MILLISECONDS);
						

						
					} else {
						MyLogger.log.log(Level.WARNING, "Handling SetContextStateCommand FAILED");
						notificationQueue.offer(new OperationStateChangedNotification(mdibOp.getMdsHandle(), mdibOp, InvocationState.FAILED), 
								500, TimeUnit.MILLISECONDS);
					}
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	

	

	
	/**
	 * This method prepares a given AbstractMetricState (which can be NumericMetricState or StringMetricState)
	 * with the new value from the AbstractSet Message
	 * @param setValueMessage 
	 * @param ms AbstractMetricState (NumericMetricState and StringMetricState are supported)
	 */
	protected boolean changeSettingValue(AbstractSet setValueMessage, AbstractMetricState ms) {
		
		//currently we can handle numeric and string value
		AbstractMetricValue value = null;
		if(ms instanceof NumericMetricState){
			value =((NumericMetricState) ms).getObservedValue();
			if (value==null){
				value=new NumericValue();
			}	
		}
		else if(ms instanceof StringMetricState){
			value = ((StringMetricState) ms).getObservedValue();
			if (value==null){
				value=new StringMetricValue();
			}
		}
		else{ 
			MyLogger.log.log(Level.WARNING, "Tried to change a value of an unsupported type!");
			return false;
		}
		
		if( value != null) {
			//TODO have a look at util class
			//this part is the same for all MetricStates
			MeasurementState measurementState=new MeasurementState();
			measurementState.setState(MetricMeasurementState.VALID);
			value.setMeasurementState(measurementState);
			value.setTimeOfObservation(BigInteger.valueOf(System.nanoTime()));
			
	
			if(ms instanceof NumericMetricState){
				((NumericValue) value).setValue(((SetValue) setValueMessage).getValue());
				((NumericMetricState) ms).setObservedValue((NumericValue) value);
			}
			else if(ms instanceof StringMetricState){
				//if it is an enum we have to check whether it is an allowed value 
				if(ms instanceof EnumStringMetricState){
					String handle = ms.getReferencedDescriptor();
					
					//search the descriptor to get the allowed values
					EnumStringMetricDescriptor descr = null;
					for (MDSDescriptor mds : description.getMDSDescriptors()) {
						if (mds instanceof HydraMDSDescriptor) {
							HydraMDSDescriptor hydraMDS = (HydraMDSDescriptor) mds;
							for (VMDDescriptor vmd : hydraMDS.getVMDs()) {
								for (ChannelDescriptor channel : vmd.getChannels()) {
									for (MetricDescriptor metric : channel.getMetrics()) {
										if (metric.getHandle().equals(handle)) {
											descr = (EnumStringMetricDescriptor) metric;
										}
									}
								}
							}
						}
					}
					
					if(descr != null){
						List<String> allowedValues = descr.getAllowedValues();
						if(allowedValues.indexOf(((SetString) setValueMessage).getString()) == -1){
							MyLogger.log.log(Level.WARNING, "The new value is NOT allowed of the requested EnumStringMetric! (Value: " + ((SetString) setValueMessage).getString() + ")" );
							return false;
						}
					}
					else{
						MyLogger.log.log(Level.SEVERE, "Did not find a Descriptor for this State!");
						return false;
					}
									
				}
				
				((StringMetricValue) value).setValue(((SetString) setValueMessage).getString());
				((StringMetricState) ms).setObservedValue((StringMetricValue) value);
			}
		}
		
		return true;		
	}

	



	public void setStates(MDState states) {
		this.states = states;
	}


	public void setDescription(MDDescription description) {
		this.description = description;
	}
	
}
