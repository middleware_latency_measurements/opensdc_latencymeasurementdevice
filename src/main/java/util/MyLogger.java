package util;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Martin Kasparick (University of Rostock - Institute of Applied Microelectronics and Computer Engineering)
 * @note Free for use in OR.NET context. (Please keep the original author(s) information in your projects.)
 */
public class MyLogger {
	
	
	/// change this constant to change the intensity of loggin ingormation
	public static final Level LOG_LEVEL = Level.FINE;
	
	private static MyLogger myLogger = null;
	public static final Logger log = Logger.getLogger( MyLogger.class.getName() );
	private Handler logHandler;
	
	static {
		//make the initialization
		getInstance();
	}

	private MyLogger() {
		log.setUseParentHandlers(false);
		
		logHandler = new ConsoleHandler();
		logHandler.setFormatter(new BriefFormatter());
		logHandler.setLevel(LOG_LEVEL);
		log.addHandler(logHandler);
		log.setLevel(LOG_LEVEL);
	}
	
	public static MyLogger getInstance(){
		if(myLogger == null){
			myLogger = new MyLogger();
		}
		return myLogger;
	}

}
