package util;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.draeger.medical.biceps.common.model.MeasurementState;
import com.draeger.medical.biceps.common.model.MetricMeasurementState;
import com.draeger.medical.biceps.common.model.NumericValue;
import com.draeger.medical.biceps.common.model.StringMetricValue;

/**
 * 
 * @author Parts or complete functionality copied from the openSDC Beta-01 tutorial and / or OR.NET openSDC tutorial workshop 
 * @author Martin Kasparick (University of Rostock - Institute of Applied Microelectronics and Computer Engineering)
 * @note Free for use in OR.NET context. (Please keep the original author(s) information in your projects.)
 */
public class Util {
	

	/**
	 * Written during openSDC tutorial in Berlin
	 * @param value
	 * @param mms
	 * @return
	 */
	public static NumericValue createNumericValue(BigDecimal value, MetricMeasurementState mms){
		MeasurementState ms = new MeasurementState();
		ms.setState(mms);
		
		NumericValue nv = new NumericValue();
		nv.setValue(value);
		nv.setMeasurementState(ms);
		nv.setTimeOfObservation(BigInteger.valueOf(System.nanoTime()));
		
		return nv;
	}
	
	public static StringMetricValue createStringMetricValue(String value, MetricMeasurementState mms){
		MeasurementState ms = new MeasurementState();
		ms.setState(mms);
		
		StringMetricValue smv = new StringMetricValue();
		smv.setValue(value);
		smv.setMeasurementState(ms);
		smv.setTimeOfObservation(BigInteger.valueOf(System.nanoTime()));
		
		return smv;
	}
	
	/**
	 * Method appends the add String to the origin String separated be the separator 
	 * @param origin
	 * @param add
	 * @param separator
	 * @return
	 */
	public static String append(String origin, String add, String separator){
		if(origin.isEmpty()){
			return add;
		} else {
			return origin + separator + add;
		}
	}
}
